
# connect to sg

import shotgun_api3 

# server_url , for exmaple 'https://mystudio.shotgunstudio.com'
# script_name, for example 'pipeline_script'
# script_key , for example '7b68e44e2fe56c579892201308ae11496693c37b749e16a50256488d616009f0'
sg = shotgun_api3.Shotgun(server_url, script_name, script_key)

# asking stuff to SG
# you need to pass the type of entity you want to list, a list of filters to get the entities you want, and the fields you want to get for each
# entity. Have in mind that by default it only lists fields type and id, and the more fields you ask for, the longer in takes.
# check the names of the fields because they may be different from what is shows in the website
# the method sg.find returns a list of entities found (or an empty list if none matches your criteria)
# each entity is represented as a dictionary of fields and values, for example {'type': 'Task', 'id': 1234}


# ----------------------------------------------------------------------------------
# get a list of tasks with a status
entity = 'Task'
filters = [
    ['sg_status_list', 'is', 'cmp'],  # here you specify that the status must be complete, you can add more conditions to this list
    ]
fields = ['content']  # asking to respond with the task name too
tasks = sg.find(entity, filters, fields)


# ----------------------------------------------------------------------------------
# Now that you have the tasks, lets get the versions from those tasks
entity = 'Version'
filters = [
    ['sg_task', 'in', tasks],  # here you specify that the the versions must be linked to the tasks you listed before
    ['sg_status_list', 'is', 'app'],  # here you specify that the status of the VERSION must be approved
    ]
fields = ['sg_task', 'sg_uploaded_movie']
versions = sg.find(entity, filters, fields)

# note: you can do this two queries in one, but the filters will be more complex, and for learning, this is more clear
# but later on, you porablably should do it in one query


# ----------------------------------------------------------------------------------
# get videos from versions, this involves, downloading, so lets go one by one. You
# can multithread this later...

# define a download folder
folder = 'C:\\temp'

# check folder exists, if not create it
if not os.path.isdir(folder):
    os.makedirs(folder)

import urllib

# loop through versions downloading its uploaded movie if they have one
for version in versions:

    # check we have an uploaded movie. If it does not, field will be None instead of a dictionary
    uploaded = version['sg_uploaded_movie']
    if not uploaded:
        msg = 'Version {} has no sg_uploaded_movie'.format(version['id'])
        print(msg)
        continue

    # get the url from the uploaded movie
    url = uploaded['url']

    # request the data for the uploaded movie to SG with the url
    request = urllib.request.Request(url)
    response = urllib.request.urlopen(request, timeout=500)

    # get the file basename from the response
    url_path = urllib.parse.urlparse(response.geturl()).path
    final_basename = os.path.basename(url_path)

    # build the path with the folder and the basename
    path = os.path.join(folder, final_basename)

    # write response to disk
    with open(path, 'wb') as open_f:
        open_f.write(response.read())
